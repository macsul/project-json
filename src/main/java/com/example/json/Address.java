package com.example.json;

import java.util.StringJoiner;

public class Address {

  /*  "address": {
        "street": "100 Maint St.",
                "city": "Philadephia",
                "state": "Pensylvania,",
                "zip": 19103,
                "country": "USA"

                */

  private  String street;
  private  String city;
  private  String state;
  private int zip;
  private String country;

    @Override
    public String toString() {
        return new StringJoiner(", ", Address.class.getSimpleName() + "[", "]")
                .add("street='" + street + "'")
                .add("city='" + city + "'")
                .add("state='" + state + "'")
                .add("zip=" + zip)
                .add("country='" + country + "'")
                .toString();
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getZip() {
        return zip;
    }

    public void setZip(int zip) {
        this.zip = zip;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }







}

