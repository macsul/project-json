package com.example.json;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class Driver {

    public static void main(String[] args) {

        ObjectMapper mapper = new ObjectMapper();

        try {

            Student student = mapper.readValue(new File("data/sample-lite.json"), Student.class);

            System.out.println("firstName"  + student.getFirstName());
            System.out.println("lastName" + student.getLastName());

            Student student1 = mapper.readValue(new File("data/sample-full.json"), Student.class);

            System.out.println("adress..." + student1.getAddress());



                    }

        catch (IOException e){

            e.printStackTrace();



        }
    }


}
