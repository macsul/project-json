package com.example.bookshelf.storage;


import com.example.bookshelf.type.Book;

import java.util.ArrayList;
import java.util.List;
public class StaticListBookStorageImpl implements  BookStorage{


    private static List<Book> bookStorage = new ArrayList<Book>();

    public StaticListBookStorageImpl() {

        bookStorage.add(new Book(10, "Harry Potter","J.K", 500,2002,"publishing company ###"));
        bookStorage.add(new Book(20, "Harry Potteri i komnata","J.K", 300,2003,"publishing company ###"));
    }

    public Book getBook(long id) {
        for (Book book : bookStorage){
            if (book.getId() == id){
                return book;
            }
        }
        return null;
    }
    public List<Book> getAllBooks() {
        System.out.println("sda");
        return bookStorage;
    }


    public void addBook(Book book) {
        bookStorage.add(book);
    }


    public void remove (long bookIdToDelete){

        for(Book book : bookStorage){
            if(book.getId() == bookIdToDelete){
                bookStorage.remove(book);
                break;

            }
        }

    }




}