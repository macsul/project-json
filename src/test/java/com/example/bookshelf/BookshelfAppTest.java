package com.example.bookshelf;

import io.restassured.RestAssured;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.when;
import static io.restassured.RestAssured.with;
import static org.hamcrest.core.StringStartsWith.startsWith;

public class BookshelfAppTest {

    private static final String BOOK_1 = "{\n" +
            "        \"id\": 10,\n" +
            "        \"title\": \"Harry Potter\",\n" +
            "        \"author\": \"J.K\",\n" +
            "        \"pagesSum\": 500,\n" +
            "        \"yearOfPublished\": 2002,\n" +
            "        \"publishingHouse\": \"publishing company ###\"\n" +
            "    }";

    private static final String BOOK_2 = "{\n"+
        "        \"id\": 20,\n"+
        "        \"title\": \"Harry Potteri i komnata\",\n"+
        "        \"author\": \"J.K\",\n"+
        "        \"pagesSum\": 300,\n"+
        "        \"yearOfPublished\": 2003,\n"+
        "        \"publishingHouse\": \"publishing company ###\"\n"+
        "    }";




   private static final int APP_PORT = 8090;

   private BookshelfApp bookshelfApp;



    @BeforeAll
    public static void BeforAll(){
        RestAssured.port APP_PORT;

    }

    @BeforeEach
    public void BeforeEach() throws Exception {

        bookshelfApp = new BookshelfApp(APP_PORT);

    }

    @AfterEach
    public void afterEach() {
        bookshelfApp.stop();
    }

    @Test
    public void addMethod() {
       // with().body(BOOK_1).when()post("book/add").then()
          //  .statusCode(200).body(equelsTo("success"));

        with().body(BOOK_1).when().post("book/add").then().statusCode(200).body(startsWith("Book has been"));


        }

    @Test
    public void addMethod500() {


        with().body(BOOK_2).when().post("book/add").then().statusCode(500);


    }

    @Test
    public void addMethod_unexpected() {

        with().body("{\"numberOfChapters\":10}").when().post("book/add").then().statusCode(500);
    }


 /*   private long addBookAndGetId(String json){
        String responseText = with().body(json)
                .when().post("/book/add").
                        then().statusCode(200)
                .body(startsWith("Book has been successfully added, id="))
                .extract().body().asString();
        String idStr = responseText.substring(responseText.indexOf("=")+1)
        return Long.parseLong(idStr);
    }


    @Test
    void getMethod_correctBookIdParam_shouldReturnStatus200(){
        Long bookId1 = addBookAndGetId(BOOK_1);
        long bookId2 = addBookAndGetId(BOOK_2);
        with().param("bookId", bookId1)
                .when().get("/book/get")
                .then().statusCode(200)
                .body("id", equalTo(bookId1))
                .body("title", equalTo("Book fake @@"))
                .body("author", equalTo("Author fake"))
                .body("pagesSum", equalTo(133))
                .body("yearOfPublished", equalTo(1947))
                .body("publishingHouse", equalTo("Wydawnictwo fake"));
    }*/
}
